Flacue
======

A utility for displaying, editing, and generating valid cuesheets for whole-album FLAC audio.

**Problem**: you ripped a CD or purchased a lossless digital copy of a album, and got one file per track. But you like whole-album files with embedded cuesheets and metadata.

Although there are utilities for joining flac-encoded tracks to a single file and attaching cuesheets, cover art and other metadata, there are no utilities for _generating_ a valid cuesheet available on your operating system.

Or maybe you want to automate the process of combining the flacs and embedding metadata, but the software you use to do this is a GUI with lots of fiddly clicky menus and dialog boxes and it wants you to enter it all manually. Or at best provides some dodgy freedb or musicbrainz fetcher. But you already have the metadata. You just don't want to have to manually copy it from the source files to the new album. You want to automate this with a shell script, maybe even run it right after ripping.

If you wanted to edit a spreadsheet you'd open friggin' Excel. Music is supposed to be pleasure, not drudgery!

**Solution**: flacue.

Features
--------

- **print** metadata from embedded cuesheets in a variety of useful and pleasant formats (see [example_output](./example_output))
- **clean up** janky non-standard embedded cuesheets that don't work in most music players (not that many players support them *at all*...)
- **import** cuesheets you found on the internet (sus!)
- **generate** valid, properly indexed cuesheets from individual track files before joining the tracks
- **edit** embedded cuesheets in your preferred code editor (and save them right back to the flac file)
- **extract** cuesheets from flac files without a complex `metaflac` invocation

Installation
------------

Clone the repository, then:

```sh
cargo build && cargo install
```

Optional: to generate a smaller, slightly faster optimized binary and install it to `/usr/local/bin/flacue`:

```sh
make && sudo make install
```

The latter requires `rust-src` via cargo, as well as usual development tools like `make` and `strip`.

Usage
-----

Usage is pretty straightforward. See `flacue --help` for options and examples.

Extra
-----

The `extra` folder contains a `joinflac` shell script that automates the whole process of joining flac files, generating and embedding cuesheets and cover art.

It will probably require a little editing to suit your needs, so you'll need to do that and stick it somewhere in your `$PATH` to use it.

It also requires [shntool](http://shnutils.freeshell.org/shntool/) and [metaflac](https://xiph.org/flac/documentation_tools_metaflac.html) to do the parts that flacue doesn't handle.

```sh
cd to/some/album/folder
joinflac *.flac
```

`joinflac` looks for cover art in the form of `cover.jpg`, `thumbnail.png`, `cd1.png`, and the like and attaches it automatically if found. It will also search for a pre-existing cue file and extract metadata from it (track names, etc.) if found, but re-generates track indices as they will be used in the joined flac.

Reporting Issues
----------------

Flacue has been tested against a library of over 270 albums in various states of completion and disrepair but due to the wide variety of ... uh, _unique_ takes on how cuesheets should be used and abused there may still be times it doesn't work as intended.

Please file bug reports on [the issue tracker](https://gitlab.com/nphyx/flacue/issues). Parsing bugs are probably caused by [cuet](https://gitlab.com/nphyx/cuet/issues) but you're welcome to report them here.

If possible include the problematic cuesheet (but **not** the music files, because DMCA and all that).

**TIP**: You can get a raw dump of an embedded cuesheet using `flacue dump my_album.flac`. It won't be parsed before dumping, so if the cuesheet is really borked this should still work.

### Known Issues

- **24-bit audio** - joinflac will crash when attempting to join 24-bit/92khz flac files. This is an upstream problem with either shntool or the flac encoder itself, with no apparent solution besides using a proprietary tool / encoder. If a fix is discovered or a working CLI encoder produced, joinflac will be updated to use it.
- **invalid characters** - some cuesheets contain invalid characters in non ansi/utf-8 encodings (particularly common with old, non-english cuesheets). When these are encountered the line containing them will be skipped, usually resulting in missing metadata in the cleaned cuesheet. Fixing them requires manual intervention (try `flacue dump` to extract the raw sheet and then `flacue attach` to attach the amended one). It may be possible to detect and replace these automatically but that is outside the scope of this project for now.

### Non-Issues

Flacue does not have a GUI frontend, probably doesn't work in Windows, does not try to look up metadata from remote sources, does not make toast, and will not wash your dog. Please don't ask for any of these features.

It *may* eventually get an interactive shell or TUI as an alternative to opening raw cuesheets in an editor, but for now this is bloat (maybe a different program?)

Ideas for other print formats are welcome, as long as they don't involve direct export to some complex or proprietary format (e.g. pdf or excel spreadsheet - instead of that, try using panadoc on markdown exports to get the format you want).

License
-------

[GPL v3](LICENSE)
