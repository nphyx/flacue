use cuet::CueSheet;
use std::cmp::max;

const SHORT_HELP: &str = r#"
Usage: flacue [OPTIONS] <flac file(s)>
       flacue --help for full help
"#;

const HELP: &str = r#"
Usage: flacue <command> [OPTIONS] <flac file(s)>

COMMANDS

d[ump]     dump raw cuesheet(s) from input file(s)
p[rint]    pretty-print the contents of cuesheet(s) for input file(s)
e[dit]     open a cuesheet in your default $EDITOR
           optionally save the edited cuesheet to the source file
g[enerate] generate a cuesheet from individual files
a[ttach]   attach a cue file to a flac (without editing it)
c[lean]    clean up a flac's embedded cue


see flacue <command> --help for options on individual commands.

Bugs? File a report at https://gitlab.com/nphyx/flacue/issues
"#;

const HELP_PRINT: &str = r#"
Usage: flacue print [options] <flac file(s)>

Prints the contents of a cuesheet according to the --style option.

OPTIONS
-h, --help            show this help text
--style <style>       set cuesheet print style (default: standard)
        standard      parse and then print the cuesheet in standard format
        pretty        print a nicely formatted readout of the parsed cuesheet
        oneline       print a single-line summary
        markdowntable print as a markdown formatted table
        table         print as an ascii table
--width <number>      max width (for table and summary styles)


Summary and table print modes include artist, album, and track count in their
output.

Other options will print all the information stored in the cuesheet.


EXAMPLES
Print a clean, human-readable representation of the cuesheet:
> flacue print --style pretty Music/albums/*.flac

Print a summary of all your albums:
> flacue print --style table ~/Music/albums/*.flac

Print an ascii-formatted table of all your albums:
> flacue print --style table ~/Music/albums/*.flac

Save a markdown-formatted table of your albums:
> flacue print --style markdowntable ~/Music/albums/*.flac > my_albums.md
"#;

const HELP_EDIT: &str = r#"
Usage: flacue edit [options] <flac file>

Extracts the cuesheet from a file and launches it in your default $EDITOR.

OPTIONS
-h, --help            show this help text
--cue <cue file>      cuefile to import in lieu of extracting from .flac file


After you exit the editor you'll be asked whether you want to save the edited
version.

If you provide a cue file using the --cue option it will be opened in the editor
instead of extracting the cuesheet from the provided flac, and the edited result
optionally saved to the file on exit.


EXAMPLES
Edit a flac's cuesheet:
> flacue edit my_album.flac

Import a .cue file, edit it, then optionally save it to the flac file:
> flacue edit --cue my_album.cue my_album.flac
"#;

const HELP_ATTACH: &str = r#"
Usage: flacue attach [options] <flac file>

Attaches a supplied cuesheet to a flac. If no cue file is supplied via the --cue
option, then it will be extracted from the flac if one is present.

OPTIONS
-h, --help            show this help text
--cue <cue file>      cuefile to import in lieu of extracting from .flac file


You must provide a cue file using the --cue option. The cue will be cleaned and
you will be prompted for confirmation before it is saved.
"#;

const HELP_CLEAN: &str = r#"
Usage: flacue clean [options] <flac file>

Cleans a flac's existing embedded cue, removing empty fields and standardizing
its format.

OPTIONS
-h, --help            show this help text

You will be prompted for confirmation before the altered file is saved.


NOTE
If you want to edit the cleaned cuesheet before updating the flac, use:
flacue edit <flac file>
"#;

const HELP_DUMP: &str = r#"
Usage flacue dump <flac file>

Extracts the cuesheet from the input file and dumps it to stdout.

OPTIONS
-h, --help            show this help text

EXAMPLES
Display a raw cue sheet:
> flacue dump my_album.flac

Dump a cue sheet to file:
> flacue dump my_album.flac > my_album.cue
"#;

const HELP_GEN: &str = r#"
Usage: flacue gen <flac files>

Generates a cue file for a collection of flac files.

OPTIONS
-h, --help            show this help text
--joined              create a cuesheet for a joined single-album flac
--cue <cue file>      cuefile to import in lieu of extracting information from flac


Flakue will order the files by track number if it is present in the
track's metadata. If not, it will place the tracks in the same order
as they've been added.

Track metadata will also be used to populate cuesheet metadata, but
album information may not be accurate since it will be derived from
tracks.

If the --joined option is used, the sheet will be generated as for
a combined (whole-album) copy of the individual files.

If --cue is provided it will be used as a basis for track and album metadata,
with missing values and track indexes derived from individual files. This
option is only valid when used in concert with --joined.


EXAMPLE
Create a cuesheet for an album split into single-track files:
> flacue gen my_album/*.flac

Create a cuesheet for a single-file album:
> flacue gen --joined my_album/*.flac
"#;

const UNK: &str = "UNKNOWN";

pub fn col_size(size: usize, margin: usize) -> usize {
    if size > (margin + 1) {
        return (size - margin) / 2;
    } else if size > 1 {
        return size / 2;
    }
    size
}

pub fn or_unk(opt: Option<&String>) -> String {
    if let Some(string) = opt {
        string.clone()
    } else {
        UNK.to_string()
    }
}

pub fn shorten(maybe_string: Option<&String>, max: usize) -> String {
    let mut out = maybe_string.unwrap_or(&UNK.to_string()).clone();
    if out.len() > max && max > 1 {
        out.truncate(max - 1);
        out.push('…');
    }
    out
}

pub fn capitalize(s: &str) -> String {
    let mut c = s.chars();
    match c.next() {
        None => String::new(),
        Some(f) => f.to_uppercase().collect::<String>() + c.as_str(),
    }
}

pub fn print_short_help() {
    println!("{}", SHORT_HELP);
}

pub fn print_help() {
    println!("{}", HELP);
}

pub fn print_help_print() {
    println!("{}", HELP_PRINT);
}

pub fn print_help_edit() {
    println!("{}", HELP_EDIT);
}

pub fn print_help_attach() {
    println!("{}", HELP_ATTACH);
}

pub fn print_help_clean() {
    println!("{}", HELP_CLEAN);
}

pub fn print_help_dump() {
    println!("{}", HELP_DUMP);
}

pub fn print_help_gen() {
    println!("{}", HELP_GEN);
}

pub fn print_remark(remark: &cuet::Remark) {
    println!("{:9}: {}", capitalize(&remark.remark_type.to_string().to_lowercase()), remark.text);
}

pub fn print_track(track: &cuet::Track) {
    let title: String = match &track.title {
        Some(t) => t.clone(),
        None => UNK.to_string(),
    };
    println!("Track {:2} : {}", track.number, title);
}

pub fn print_pretty(cue: &CueSheet) {
    println!("Title    : {}", or_unk(cue.title.as_ref()));
    println!("Artist   : {}", or_unk(cue.performer.as_ref()));
    cue.remarks.iter().for_each(print_remark);
    cue.tracks.values().for_each(print_track);
    println!();
}

pub fn print_oneline(cue: &CueSheet, size: usize) {
    let cs = col_size(size, 13);
    println!("{} - {} : {} tracks", shorten(cue.performer.as_ref(), cs), shorten(cue.title.as_ref(), cs), cue.tracks.len());
}

pub fn print_thead(size: usize) {
    let cs = col_size(size, 11);
    println!("| Artist{0:1$} | Album{2:3$ } | Tracks |", ' ', max(cs, 8) - 8, ' ', max(cs, 7) - 7);
    println!("|{0:-<cs$}|{0:-<cs$}|--------|", "");
}

pub fn print_mthead() {
    println!("| Artist | Album | Tracks |");
    println!("|:--|:--|:-:|");
}

pub fn print_trow(cue: &CueSheet, size: usize) {
    let cs = col_size(size, 15);
    println!("| {:cs$} | {:cs$} | {:6} |", shorten(cue.performer.as_ref(), cs), shorten(cue.title.as_ref(), cs), cue.tracks.len());
}

pub fn print_tfoot(size: usize) {
    let cs = col_size(size, 11);
    println!("|{0:_<cs$}|{0:_<cs$}|________|", "");
}

pub fn print_title(cue: &CueSheet) {
    println!("{} - {}.flac", shorten(cue.performer.as_ref(), 0), shorten(cue.title.as_ref(), 0));
}

pub fn print_md_block(cue: &CueSheet) {
    use cuet::Track;
    println!("{} - {}", shorten(cue.performer.as_ref(), 0), shorten(cue.title.as_ref(), 0));
    println!("==================================");
    println!();
    for remark in &cue.remarks {
        println!("*{}: {}*\n", capitalize(&remark.remark_type.to_string().to_lowercase()), remark.text);
    }
    if cue.songwriter.is_some() { println!("*Songwriter: {}*\n", cue.songwriter.as_ref().unwrap()); }
    if cue.catalog.is_some() { println!("*Catalog Number: {}*\n", cue.catalog.as_ref().unwrap()); }
    let mut tracks = cue.tracks.values().collect::<Vec<&Track>>();
    let pad = Track::default();
    if tracks.len() % 2 == 1 {
        tracks.push(&pad);
    }
    let half_tracks = tracks.len() / 2;
    let col_a = tracks.iter().take(half_tracks);
    let col_b = tracks.iter().skip(half_tracks);

    let cols = col_a.zip(col_b);
    // println!("{} {}", col_a.len(), col_b.len());
    println!("\n|#| Title |#| Title |");
    println!("|-:|:-|-:|:-|");
    for (a, b) in cols {
        let no_title = &String::from("No Title");
        let title_a = a.title.as_ref().unwrap_or(no_title);
        let title_b = b.title.as_ref().unwrap_or(no_title);
        if b.number == 0 { // padder
            println!("|{}|{}|||", a.number, title_a);
        }
        else {
            println!("|{}|{}|{}|{}|", a.number, title_a, b.number, title_b);
        }
    }
    println!();
}
