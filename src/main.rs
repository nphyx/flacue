use std::process::ExitCode;
mod options;
mod print;
mod io;
mod error;
use error::*;
use options::{*,PrintStyle,PrintStyle::*};
use print::*;
use cuet::*;


fn prompt(name:&str) -> Result<String, FlakueError> {
    use std::io::{Write,stdout,stdin};
    let mut line = String::new();
    print!("{}", name);
    stdout().flush()?;
    stdin().read_line(&mut line)?;

    Ok(line.trim().to_string())
}

fn print_cue(cue: &CueSheet, width: usize, style: PrintStyle) {
    match style {
        Pretty => print_pretty(cue),
        Standard => println!("{}", cue),
        OneLine => print_oneline(cue, width),
        Table => print_trow(cue, width),
        MarkdownTable => print_trow(cue, 0),
        MarkdownBlock => print_md_block(cue),
        Title => print_title(cue),
    }
}

fn handle_edit(opts: &Options) -> Result<(), FlakueError> {
    use Subcommand::{Attach, Edit, Clean};
    if opts.help || opts.short_help {
        match opts.subcommand {
            Edit => print_help_edit(),
            Attach => print_help_attach(),
            Clean => print_help_clean(),
            _ => {},
        }
        return Ok(());
    }
    if opts.files.len() > 1 {
        println!("You may edit only one file at a time!");
        return Err(FlakueError);
    }

    if opts.subcommand == Attach && opts.cuefile.is_none() {
        println!("You must provide a cue file via --cue option.");
        return Err(FlakueError);
    }

    let mut sheet: CueSheet;
    let filename = opts.files.get(0).ok_or_else(|| {
        println!("You must supply a flac file to save your edited sheet to!");
        FlakueError
    })?;

    if opts.cuefile.is_some() {
        sheet = io::read_cue_from_cuefile(opts.cuefile.as_ref().unwrap())?;
    } else {
        sheet = match io::read_cue_from_flac(filename) {
            Ok(s) => s,
            Err(_) => CueSheet::default(),
        }
    }
    if opts.subcommand == Subcommand::Edit {
        sheet.replace_from_string(&edit::edit(sheet.to_string())?);
    }
    if sheet.nonstandard {
        println!("{} lines were not recognized and will be omitted.", sheet.get_skipped_count());
    }
    if opts.subcommand == Subcommand::Attach {
        println!("{}", sheet);
    }
    let response = prompt(&format!("Write to {}? [y/N] ", &filename))?;

    if response == "y" {
        print!("Saving...");
        io::save_cuesheet(filename, &sheet)?;
        println!("Done.");
    } else {
        println!("Canceled.");
        return Err(FlakueError);
    }
    Ok(())
}

fn handle_dump(opts: &Options) -> Result<(), FlakueError> {
    if opts.help || opts.short_help {
        print_help_dump();
        return Ok(());
    }
    use std::io::prelude::*;
    if let Some(cuefile) = &opts.cuefile {
        for line in  io::read_from_cuefile(cuefile)?.lines() {
            println!("{}", line?);
        }
    }
    for filename in &opts.files {
        if let Ok(string) = io::read_from_flac(filename) {
            println!("{}", string);
        }
    }
    Ok(())
}

fn handle_print(opts: &Options) -> Result<(), FlakueError> {
    if opts.help || opts.short_help {
        print_help_print();
        return Ok(());
    }

    for filename in &opts.files {
        if !std::path::Path::new(filename).is_file() {
            println!("{}: file does not exist", filename);
            return Err(FlakueError);
        }
    }

    let mut sheet = CueSheet::default();
    let mut blank_sheet = CueSheet::default();

    if opts.style == Table {
        print_thead(opts.width);
    }
    if opts.style == MarkdownTable {
        print_mthead();
    }

    for filename in &opts.files {
        if let Ok(string) = io::read_from_flac(filename) {
            sheet.replace_from_string(&string);
            print_cue(&sheet, opts.width, opts.style);
        } else {
            blank_sheet.title = Some(filename.clone());
            print_cue(&blank_sheet, opts.width, opts.style);
        }
    }

    if opts.style == Table {
        print_tfoot(opts.width);
    }

    Ok(())
}

fn handle_help_only(opts: &Options) -> Result<(), FlakueError> {
    if opts.help {
        print_help();
    } else {
        print_short_help();
    }
    Ok(())
}

fn get_vorbis_prop(tag: &metaflac::Tag, prop: &str) -> Option<String> {
    if let Some(mut iter) = tag.get_vorbis(prop) {
        return Some(iter.next()?.to_string());
    }
    None
}

struct TrackBundle {
    number: usize,
    length: u64,
    track: Track,
    file: File,
}

impl TrackBundle {
    fn new(number: usize, length: u64, track: Track, file: File) -> Self {
        TrackBundle{
            number,
            length,
            track,
            file,
        }
    }
}

fn handle_gen(opts: &Options) -> Result<(), FlakueError> {
    if opts.help || opts.short_help {
        print_help_gen();
        return Ok(());
    }

    if opts.cuefile.is_some() && !opts.joined {
        println!("The --cue option requires the --joined option when using [g]en command.");
        return Err(FlakueError);
    }

    let mut sheet = match &opts.cuefile {
        Some(file) => {
            if !std::path::Path::new(file).exists() {
                println!("Provided cue file {} does not exist!", file);
                return Err(FlakueError);
            }
            let orig = io::read_cue_from_cuefile(file)?;
            let mut out = CueSheet::default();
            out.text_file = orig.text_file;
            out.songwriter = orig.songwriter;
            out.catalog = orig.catalog;
            out.performer = orig.performer;
            out.title = orig.title;
            out.remarks = orig.remarks;
            out.tracks = orig.tracks;
            out
        }
        None => CueSheet::default(),
    };
    let mut bundles: Vec<TrackBundle> = Vec::new();
    for (i, filename) in opts.files.iter().enumerate() {
        let tag = io::read_flac_meta(filename)?;
        let track_num;
        if let Some(num) = get_vorbis_prop(&tag, "TRACKNUMBER") { //.unwrap_or_else(|| i.to_string());
            if let Ok(n) = num.parse::<usize>() {
                track_num = n;
            } else {
                track_num = i + 1;
            }
        } else {
            track_num = i + 1;
        }
        let mut track = match sheet.tracks.get(&track_num) {
            Some(t) => Track{
                indices: Default::default(),
                ..t.clone()
            },
            None => Track::new(TrackType::Audio),
        };
        track.number = track_num;

        let file = File::new(FileType::Wave, filename.clone());
        let mut length: u64 = 0;
        // compute duration
        if let Some(block) = tag.get_streaminfo() {
            length = (block.total_samples * 1000) / u64::from(block.sample_rate);
        }
        // get album title if we don't already have it
        if sheet.title.is_none() {
            sheet.title = get_vorbis_prop(&tag, "ALBUM");
        }
        if sheet.performer.is_none() {
            sheet.performer = get_vorbis_prop(&tag, "ARTIST");
        }
        // track props
        if track.title.is_none() { track.title = get_vorbis_prop(&tag, "TITLE"); }
        if track.performer.is_none() { track.performer = get_vorbis_prop(&tag, "ARTIST"); }
        if track.isrc.is_none() { track.isrc = get_vorbis_prop(&tag, "ISRC"); }
        if let Some(text) = get_vorbis_prop(&tag, "DESCRIPTION") {
            track.append_remark(RemarkType::Comment, text);
        }
        if let Some(text) = get_vorbis_prop(&tag, "GENRE") {
            track.append_remark(RemarkType::Genre, text);
        }
        if let Some(text) = get_vorbis_prop(&tag, "DATE") {
            track.append_remark(RemarkType::Date, text);
        }
        bundles.push(TrackBundle::new(track.number, length, track, file));
    }
    let mut total_millis: u64 = 0;
    bundles.sort_by(|a, b| a.number.cmp(&b.number));
    let mut joined_file = File::new(FileType::Wave, "joined.flac".into());
    for mut bundle in bundles {
        let mut point: CuePoint = Default::default();
        if opts.joined {
            point = CuePoint::try_from_duration(
                std::time::Duration::from_millis(total_millis)
            )?;
            total_millis += bundle.length;
        }
        bundle.track.indices.push(Index::new(1, point));
        sheet.append_track(bundle.track);
        if opts.joined {
            joined_file.tracks.push(bundle.number);
        } else {
            bundle.file.tracks.push(bundle.number);
            sheet.append_file(bundle.file);
        }
    }
    if opts.joined {
        joined_file.name = format!(
            "{} - {}.flac",
            sheet.performer.clone().unwrap_or_else(|| "UNKNOWN".into()),
            sheet.title.clone().unwrap_or_else(|| "UNKNOWN".into())
        );
        sheet.append_file(joined_file);
    }
    println!("{}", sheet);
    Ok(())
}

fn main() -> std::process::ExitCode {
    if let Ok(opts) = Options::try_from_env() {
        use options::Subcommand::*;
        if (match opts.subcommand {
            Generate => handle_gen(&opts),
            Dump => handle_dump(&opts),
            Edit | Attach | Clean => handle_edit(&opts),
            Print => handle_print(&opts),
            Error => handle_help_only(&opts),
        }).is_err() {
            print_short_help();
            return ExitCode::FAILURE;
        }
        ExitCode::SUCCESS
    } else {
        print_short_help();
        ExitCode::FAILURE
    }
}
