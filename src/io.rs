use metaflac::{Tag, Block, BlockType};
use cuet::CueSheet;
use std::path::Path;
use std::fs::File;
use std::io::BufReader;
use crate::error::FlakueError;

pub fn read_from_cuefile(filename: &String) -> Result<BufReader<File>, std::io::Error> {
    let path = Path::new(filename);
    assert!(path.is_file(), "cue file does not exist");
    let file = File::open(path)?;
    Ok(BufReader::new(file))
}

pub fn read_cue_from_cuefile(filename: &String) -> Result<CueSheet, std::io::Error> {
    Ok(CueSheet::from(read_from_cuefile(filename)?))
}

pub fn read_flac_meta(filename: &String) -> Result<Tag, metaflac::Error> {
    Tag::read_from_path(&filename)
}

pub fn find_cuesheet_block(tag: Tag) -> Option<metaflac::block::VorbisComment> {
    let mut blocks = tag.get_blocks(BlockType::VorbisComment);
    if let Some(Block::VorbisComment(block)) = blocks.next() {
        if block.get("CUESHEET").is_some() {
            return Some(block.to_owned());
        }
    }
    None
}

/*
pub fn get_vorbis_block_index(tag: &Tag) -> Option<usize> {
    let blocks = tag.get_blocks(BlockType::VorbisComment).enumerate();
    for (i, block) in blocks {
        if let Block::VorbisComment(_) = block {
            return Some(i)
        }
    }
    None
}

pub fn has_cuesheet_block(tag: &Tag) -> bool {
    let blocks = tag.get_blocks(BlockType::VorbisComment);
    for block in blocks {
        if let Block::VorbisComment(b) = block {
            if b.get("CUESHEET").is_some() {
                return true;
            }
        }
    }
    false
}
*/

pub fn get_cuesheet(tag: Tag) -> Option<String> {
    if let Some(sheet) = find_cuesheet_block(tag)?.get("CUESHEET")
        .and_then(|v| v.first()) {
        return Some(sheet.to_owned());
    }
    None
}

pub fn save_cuesheet(filename: &String, cue: &CueSheet) -> Result<(), FlakueError> {
    if !Path::new(filename).exists() {
        println!("output file not found");
        return Err(FlakueError);
    }
    if let Ok(tag) = &mut read_flac_meta(filename) {
        tag.set_vorbis("CUESHEET", vec![cue.to_string()]);
        if tag.write_to_path(filename).is_err() {
            println!("failed to write tag to output :( Dumping cuesheet to stdout...");
            println!("{}", cue);
        }
        /*
        if has_cuesheet_block(tag) {
            println!("Replacing existing cuesheet...");
        } else if let Some(i) = get_vorbis_block_index(tag) {
            let mut blocks: Vec<&Block> = tag.get_blocks(BlockType::VorbisComment).collect();
            if !blocks.is_empty() {
                println!("Appending cuesheet to vorbis comments...");
                tag.set_vorbis("CUESHEET", vec![cue.to_string()]);
            }
        }
        // need to create a new block
        println!("Inserting new cuesheet...");
        */
    }
    Ok(())
}

pub fn read_from_flac(filename: &String) -> Result<String, FlakueError> {
    get_cuesheet(read_flac_meta(filename)?).ok_or(FlakueError)
}

pub fn read_cue_from_flac(filename: &String) -> Result<CueSheet, FlakueError> {
    Ok(CueSheet::from(read_from_flac(filename)?))
}
