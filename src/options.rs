use termsize::get as tsize;
use crate::error::FlakueError;

#[derive(Copy,Clone,Default,PartialEq,Eq)]
pub enum PrintStyle {
    MarkdownBlock,
    MarkdownTable,
    OneLine,
    Pretty,
    #[default]
    Standard,
    Table,
    Title,
}

impl From<&str> for PrintStyle {
    fn from(string: &str) -> PrintStyle {
        match string {
            "standard" => PrintStyle::Standard,
            "pretty" => PrintStyle::Pretty,
            "oneline" => PrintStyle::OneLine,
            "table" => PrintStyle::Table,
            "markdowntable" => PrintStyle::MarkdownTable,
            "md" => PrintStyle::MarkdownBlock,
            "title" => PrintStyle::Title,
            _ => PrintStyle::Standard,
        }
    }
}

#[derive(Default,Copy,Clone,PartialEq,Eq)]
pub enum Subcommand {
    Dump,
    Edit,
    Attach,
    Clean,
    Print,
    Generate,
    #[default]
    Error
}

impl From<Option<String>> for Subcommand {
    fn from(opt: Option<String>) -> Self {
        if let Some(cmd) = opt {
            return match cmd.as_str() {
                "d" | "dump" => Self::Dump,
                "e" | "edit" => Self::Edit,
                "g" | "gen" => Self::Generate,
                "p" | "print" => Self::Print,
                "a" | "attach" => Self::Attach,
                "c" | "clean" => Self::Clean,
                _ => Self::Error,
            }
        }
        Self::Error
    }
}

#[derive(Default)]
pub struct Options {
    pub subcommand: Subcommand,
    pub cuefile: Option<String>,
    pub files: Vec<String>,
    pub help: bool,
    pub short_help: bool,
    pub style: PrintStyle,
    pub width: usize,
    pub joined: bool,
}

fn get_width() -> usize {
    match tsize() {
        Some(size) => usize::from(size.cols),
        None => 80,
    }
}

impl Options {
    pub fn try_from_env() -> Result<Options, FlakueError> {
        let mut pargs = pico_args::Arguments::from_env();
        let mut options = Options {
            short_help: pargs.contains("-h"),
            help: pargs.contains("--help"),
            subcommand: pargs.subcommand()?.into(),
            ..Default::default()
        };

        match options.subcommand {
            Subcommand::Edit | Subcommand::Attach => {
                options.cuefile = pargs.value_from_str("--cue").ok();
            },
            Subcommand::Generate => {
                options.cuefile = pargs.value_from_str("--cue").ok();
                options.joined = pargs.contains("--joined");
            }
            Subcommand::Print => {
                options.style = PrintStyle::from(pargs
                                                 .value_from_str("--style")
                                                 .unwrap_or_else(|_| "standard".to_string())
                                                 .as_str());
                if let Ok(Some(w)) = pargs.opt_value_from_str::<&str, String>("--width") {
                    options.width = w.parse::<usize>().unwrap_or_else(|_| get_width());
                } else {
                    options.width = get_width();
                }
            },
            _ => {},
        }
        while let Ok(Some(file)) = pargs.opt_free_from_str::<String>() {
            if file.starts_with("--") {
                println!("Unrecognized option: {}", file);
                return Err(FlakueError);
            }
            options.files.push(file);
        }
        pargs.finish();
        Ok(options)
    }
}
