use std::error::Error;

#[derive(Debug)]
pub struct FlakueError;

// lazy...
impl<T> From<T> for FlakueError where T: Error {
    fn from(_: T) -> Self {
        Self
    }
}
