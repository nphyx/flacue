The Smashing Pumpkins - Mellon Collie and The Infinite Sadness (Disc 1 - Dawn to Dusk)
==================================

*Genre: Alt. Rock*

*Date: 1995*

*Comment: YEAR: 1995*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|Mellon Collie and The Infinite Sadness|8|Fuck You (An Ode To No One)|
|2|Tonight, Tonight|9|Love|
|3|Jellybelly|10|Cupid de Locke|
|4|Zero|11|Galapogos|
|5|Here Is No Why|12|Mmuzzle|
|6|Bullet With Butterfly Wings|13|Porcelina of The Vast Oceans|
|7|To Forgive|14|Take Me Down|

The Smashing Pumpkins - Oceania
==================================

*Genre: Rock*

*Date: 2012*

*Comment: YEAR: 2012*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|Quasar|8|Oceania|
|2|Panopticon|9|Pale Horse|
|3|The Celestials|10|The Chimera|
|4|Violet Rays|11|Glissandra|
|5|My Love Is Winter|12|Inkless|
|6|One Diamond, One Heart|13|Wildflower|
|7|Pinwheels|||

The Smashing Pumpkins - Zeitgeist
==================================

*Genre: Rock*

*Date: 2007*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|Doomsday Clock|7|United States|
|2|7 Shades Of Black|8|Neverlost|
|3|Bleeding The Orchid|9|Bring The Light|
|4|That's The Way (My Love Is)|10|(Come On) Let's Go!|
|5|Tarantula|11|For God And Country|
|6|Starz|12|Pomp And Circumstances|

The Smashing Pumpkins - Pisces Iscariot
==================================

*Genre: Alternative*

*Date: 1994*

*Comment: YEAR: 1994*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|Soothe|8|Obscured|
|2|Frail And Bedazzled|9|Landslide|
|3|Plume|10|Starla|
|4|Whir|11|Blue|
|5|Blew Away|12|Girl Named Sandoz|
|6|Pissant|13|La Dolly Vita|
|7|Hello Kitty Kat|14|Spaced|

The Smashing Pumpkins - Machina II (Disc 2)
==================================

*Genre: Alternative*

*Date: 2000*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|Glass|8|Home|
|2|Cash Car Star|9|Blue Skies (version electrique)|
|3|Dross|10|White Spyder|
|4|Real Love|11|In My Body|
|5|Go|12|If There Is A God|
|6|Let Me Give The World To You|13|Le Deux Machina|
|7|Innosense|14|Here's To The Atom Bomb|

The Smashing Pumpkins - Adore B-Sides & Demos
==================================

*Genre: Alternative*

*Date: 1997*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|Lover|10|Winterlong|
|2|Let Me Give The World To You|11|Soot and Stars|
|3|Because You Are|12|Chewing Gum|
|4|Satellites|13|Do You Close Your Eyes?|
|5|Without You|14|My Mistake|
|6|Joy|15|Jersey Shore|
|7|Blissed and Gone (cut)|16|Valentine|
|8|Saturnine|17|Waiting (cut)|
|9|Cross (cut)|||

The Smashing Pumpkins - Machina II (Disc 1)
==================================

*Genre: Alternative*

*Date: 2000*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|Slow Dawn|7|Lucky 13|
|2|Vanity|8|Speed Kills But Beauty Lives Forever|
|3|Satur9|9|If There Is A God (piano and voice)|
|4|Glass (alternate version)|10|Try (version 1)|
|5|Soul Power|11|Heavy Metal Machine (version 1/alternate mix)|
|6|Cash Car Star (version 1)|||

The Smashing Pumpkins - Siamese Dream
==================================

*Date: 2011-12-05*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|Cherub Rock|8|Geek U.S.A.|
|2|Quiet|9|Mayonaise|
|3|Today|10|Spaceboy|
|4|Hummer|11|Silverfuck|
|5|Rocket|12|Sweet Sweet|
|6|Disarm|13|Luna|
|7|Soma|||

The Smashing Pumpkins - Earphoria
==================================

*Genre: Alternative Rock*

*Date: 1994*

*Comment: YEAR: 2002*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|Sinfony|9|Soma (Live in London, 1994)|
|2|Quiet (Live in Atlanta, 1993)|10|Slunk (Live on Japanese TV, 1992)|
|3|Disarm (Live on English TV, 1993)|11|French Movie Theme|
|4|Cherub Rock (Acoustic) (Live on MTV Europe, 1993)|12|Geek U.S.A. (Live on German TV, 1993)|
|5|Today (Live in Chicago, 1993)|13|Mayonaise (Acoustic) (Live Everywhere, 1988-1994)|
|6|Bugg Superstar|14|Silverfuck (Live in London, 1994) - Over the Rainbow - Jackboot|
|7|I Am One (Live in Barcelona, 1993)|15|Why Am I So Tired|
|8|Pulseczar|||

The Smashing Pumpkins - Monuments To An Elegy
==================================

*Genre: Grunge*

*Date: 2014*

*Replaygain_album_gain: -11.02 dB*

*Replaygain_album_peak: 1.000000*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|Tiberius|6|Drum + Fife|
|2|Being Beige|7|Monuments|
|3|Anaise!|8|Dorian|
|4|One And All (We Are)|9|Anti-Hero|
|5|Run2Me|||

The Smashing Pumpkins - Adore
==================================

*Genre: Alternative*

*Date: 1998*

*Comment: YEAR: 1998*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|To Sheila|9|Pug|
|2|Ava Adore|10|The Tale Of Dusty And Pistol Pete|
|3|Perfect|11|Annie-Dog|
|4|Daphne Descends|12|Shame|
|5|Once Upon A Time|13|Behold! The Night Mare|
|6|Tear|14|For Martha|
|7|Crestfallen|15|Blank Page|
|8|Appels + Oranjes|16|17|

The Smashing Pumpkins - Mellon Collie And The Infinite Sadness (Disc 2 - Twilight To Starlight)
==================================

*Genre: Alternative*

*Date: 1995*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|Where Boys Fear To Tread|8|Stumbleine|
|2|Bodies|9|X.Y.U.|
|3|Thirty-Three|10|We Only Come Out At Night|
|4|In The Arms Of Sleep|11|Beautiful|
|5|1979|12|Lily (My One And Only)|
|6|Tales Of A Scorched Earth|13|By Starlight|
|7|Thru The Eyes Of Ruby|14|Farewell And Goodnight|

The Smashing Pumpkins - Machina - The Machines of God I
==================================

*Genre: Alternative*

*Date: 2000*

*Comment: YEAR: 1999*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|The Everlasting Gaze|9|The Imploding Voice|
|2|Raindrops + Sunshowers|10|Glass And The Ghost Children|
|3|Stand Inside Your Love|11|Wound|
|4|I Of The Mourning|12|The Crying Tree Of Mercury|
|5|The Sacred And Profane|13|With Every Light|
|6|Try, Try, Try|14|Blue Skies Bring Tears|
|7|Heavy Metal Machine|15|Age Of Innocence|
|8|This Time|||

The Smashing Pumpkins - Gish
==================================

*Genre: Alternative*

*Date: 1991*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|I Am One|6|Suffer|
|2|Siva|7|Snail|
|3|Rhinoceros|8|Tristessa|
|4|Bury Me|9|Window Paine|
|5|Crush|10|Daydream|

The Smashing Pumpkins - Rotten Apples: Greatest Hits
==================================

*Genre: Alternative Rock*

*Date: 2001*


|#| Title |#| Title |
|-:|:-|-:|:-|
|1|Siva|10|Zero|
|2|Rhinoceros|11|Tonight, Tonight|
|3|Drown|12|Eye|
|4|Cherub Rock|13|Ava Adore|
|5|Today|14|Perfect|
|6|Disarm|15|The Everlasting Gaze|
|7|Landslide|16|Stand Inside Your Love|
|8|Bullet With Butterfly Wings|17|Real Love (Bonus Track)|
|9|1979|18|Untitled (Bonus Track)|

