| Artist | Album | Tracks |
|:--|:--|:-:|
| The Smashing Pumpkins | Mellon Collie and The Infinite Sadness (Disc 1 - Dawn to Dusk) |     14 |
| The Smashing Pumpkins | Oceania |     13 |
| The Smashing Pumpkins | Zeitgeist |     12 |
| The Smashing Pumpkins | Pisces Iscariot |     14 |
| The Smashing Pumpkins | Machina II (Disc 2) |     14 |
| The Smashing Pumpkins | Adore B-Sides & Demos |     17 |
| The Smashing Pumpkins | Machina II (Disc 1) |     11 |
| The Smashing Pumpkins | Siamese Dream |     13 |
| The Smashing Pumpkins | Earphoria |     15 |
| The Smashing Pumpkins | Monuments To An Elegy |      9 |
| The Smashing Pumpkins | Adore |     16 |
| The Smashing Pumpkins | Mellon Collie And The Infinite Sadness (Disc 2 - Twilight To Starlight) |     14 |
| The Smashing Pumpkins | Machina - The Machines of God I |     15 |
| The Smashing Pumpkins | Gish |     10 |
| The Smashing Pumpkins | Rotten Apples: Greatest Hits |     18 |
